# Solr ConfigSets

This repository holds the config sets for solr used in Swisscollections and Bildungsgeschichte.

## Access Container
With this command it is possible to view the structure within the container.
```bash
docker exec -it solr-container-ansible-managed bash
```

## Update ConfigSets on Zookeeper
The config sets are stored in Zookeeper. Solr then picks them from there for the collections. The config can be uploaded with
the ConfigSets API of Solr.

Steps : 

 - Update this configuration and then push it to gitlab
 - Update the server either via Ansible or manually as follows:
   - ssh to the solr server
   - switch to the solr user : `sudo su solr`
   - cd `/data/solr/config`
   - git pull

Then execute the following two commands to deploy them in `/data/solr`. After that delete the generated zip files.

In order to upload the new configset the current one has to be set to untrusted as we do not use authentication. Do so via ZooNavigator in the top level entry of each configset. 

![screenshot zoonavigator](zoonavigator-screenshot-trusted.png)

- https://zoonavigator.ub-dd-prod.k8s-001.unibas.ch

Instead of `{"trusted":true}`, write `{"trusted":false}` and save. After the configset is updated turn it back.

### Swisscollection Prod
```bash
(cd /data/solr/config/configs/swisscollection/conf && zip -r - *) > swisscollection.zip

curl -X PUT --header "Content-Type:application/octet-stream" --data-binary @swisscollection.zip --cacert /data/certs/ca.crt --key /data/certs/tls.key --cert /data/certs/tls.crt "https://ub-solr01.ub.unibas.ch:8080/api/cluster/configs/swisscollection?overwrite=true&cleanup=true" 
```

### Browsevalues Prod

```bash
(cd /data/solr/config/configs/browsevalues/conf && zip -r - *) > browsevalues.zip

curl -X PUT --header "Content-Type:application/octet-stream" --data-binary @browsevalues.zip --cacert /data/certs/ca.crt --key /data/certs/tls.key --cert /data/certs/tls.crt "https://ub-solr01.ub.unibas.ch:8080/api/cluster/configs/browsevalues?overwrite=true&cleanup=true"
```


### Swisscollection Standby
```bash
(cd /data/solr/config/configs/swisscollection/conf && zip -r - *) > swisscollection.zip

curl -X PUT --header "Content-Type:application/octet-stream" --data-binary @swisscollection.zip --cacert /data/certs/ca.crt --key /data/certs/tls.key --cert /data/certs/tls.crt "https://ub-solr03.ub.unibas.ch:8080/api/cluster/configs/swisscollection?overwrite=true&cleanup=true" 
```

### Browsevalues Standby

```bash
(cd /data/solr/config/configs/browsevalues/conf && zip -r - *) > browsevalues.zip

curl -X PUT --header "Content-Type:application/octet-stream" --data-binary @browsevalues.zip --cacert /data/certs/ca.crt --key /data/certs/tls.key --cert /data/certs/tls.crt "https://ub-solr03.ub.unibas.ch:8080/api/cluster/configs/browsevalues?overwrite=true&cleanup=true"
```

### Swisscollection Test
```bash
(cd /data/solr/config/configs/swisscollection/conf && zip -r - *) > swisscollection.zip

curl -X PUT --header "Content-Type:application/octet-stream" --data-binary @swisscollection.zip --cacert /data/certs/ca.crt --key /data/certs/tls.key --cert /data/certs/tls.crt "https://ub-solr05.ub.unibas.ch:8080/api/cluster/configs/swisscollection?overwrite=true&cleanup=true" 
```

### Browsevalues Test

```bash
(cd /data/solr/config/configs/browsevalues/conf && zip -r - *) > browsevalues.zip

curl -X PUT --header "Content-Type:application/octet-stream" --data-binary @browsevalues.zip --cacert /data/certs/ca.crt --key /data/certs/tls.key --cert /data/certs/tls.crt "https://ub-solr05.ub.unibas.ch:8080/api/cluster/configs/browsevalues?overwrite=true&cleanup=true"
```



## Create Collections

```bash
curl --cacert /data/certs/ca.crt --key /data/certs/tls.key --cert /data/certs/tls.crt -XPOST https://ub-solr01.ub.unibas.ch:8080/api/collections -H 'Content-Type: application/json' -d '
  {
    "name": "swisscollections-v1",
    "config": "swisscollection",
    "replicationFactor": 2,
    "numShards": 1
  }
'
```


```bash
curl --cacert /data/certs/ca.crt --key /data/certs/tls.key --cert /data/certs/tls.crt -XPOST https://ub-solr01.ub.unibas.ch:8080/api/collections -H 'Content-Type: application/json' -d '
  {
    "name": "browsevalues-v1",
    "config": "browsevalues",
    "replicationFactor": 2,
    "numShards": 1
  }
'
```