#!/usr/bin/env bash
#This deploys solr configuration from searchconf, update zookeeper and reload the nodes
#for the test cluster

#DEPLOY_FILE_SYSTEM_BASE=/home/swissbib/deployment
#DEPLOYUNZIPSEARCHCONF=${DEPLOY_FILE_SYSTEM_BASE}/unzipsearchconf
#SOLR_CONF_DIR=$DEPLOYUNZIPSEARCHCONF/solr/bib/solr.home
#ZOOKEEPERCONF=/usr/local/swissbib/solr/zookeeperconf
#COLLECTION_NAME=frbr

# neue Version für swisscollections
COLLECTION_NAME=swisscollection
#COLLECTION_NAME=browsevalues
DEPLOY_FILE_SYSTEM_BASE=/home/swissbib/deployment
DEPLOYUNZIPSEARCHCONF=${DEPLOY_FILE_SYSTEM_BASE}/unzipsearchconf
SOLR_CONF_DIR=$DEPLOYUNZIPSEARCHCONF/searchconf/configs/${COLLECTION_NAME}
ZOOKEEPERCONF=/opt/solr/zookeeperconf/${COLLECTION_NAME}

if [ -d ${DEPLOYUNZIPSEARCHCONF} ]; then
    rm -rf ${DEPLOYUNZIPSEARCHCONF}
fi


mkdir -p ${DEPLOYUNZIPSEARCHCONF}

cd ${DEPLOYUNZIPSEARCHCONF}

git clone https://github.com/swissbib/searchconf.git .
git checkout collapse


#one node per cluster
for host in dd-so5.ub.unibas.ch
do
    echo "deploying conf to: $host"

    # brauchts rm, mkdir Zeilen?
    ssh swissbib@${host} "[ -d ${ZOOKEEPERCONF} ]  &&  rm -rf ${ZOOKEEPERCONF}"
    ssh swissbib@${host} "mkdir -p  ${ZOOKEEPERCONF}"
    rsync -a  ${SOLR_CONF_DIR}/* swissbib@${host}:${ZOOKEEPERCONF}

    #send the configuration from the node to the zookeeper
    #funktioniert das mit den COLLECTION_NAME?
    ssh swissbib@${host} 'cd ~; source ./SOLRDEFS.sh; $SOLR_CLOUD_SCRIPT/zkcli.sh -z $ZK_HOST_NS -cmd upconfig -confdir $SOLR_BASE/zookeeperconf/${COLLECTION_NAME}/conf -confname ${COLLECTION_NAME}'

    #reload the collection
    ssh swissbib@${host} "curl 'http://localhost:8080/solr/admin/collections?action=RELOAD&name=${COLLECTION_NAME}'"

done
rm -rf ${DEPLOYUNZIPSEARCHCONF}